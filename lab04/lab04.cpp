#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <math.h>
#include <string>
#include <sstream>

using namespace std;

class Log {
	public:
		static void info(string message = "") {
			cout << message << endl<<endl;
		}
};

class Dice {
	private:
		int sides;

	public:

		Dice() {
			sides=4;
		}

		Dice(int sides) {
			this->sides = sides;
		}

		int getSides() {
			return this->sides;
		}



		int roll() {
			int result = (rand() % getSides()) + 1;

			ostringstream ss;
			ss << "Dice roll: " << result;
			Log::info(ss.str());

			return result;
		}


};


class Pawn {

	private:
		int position;
		string name;


		public:
		Pawn() {}

		Pawn(string name) {
			this->name = name;
			this->position = 0;

			Log::info(name + " joined the game.");
		}

		string getName() {
			return name;
		}

		int getPosition() {
			return position;
		}

		void setPosition(int position) {
			this->position = position;
		}

		void addToPosition(int move) {
			this->position += move;
		}

		void subtractFromPosition(int move)
		{
			this->position-=move;
		}


};

class Board {

	private:
		int maxPosition;
		vector<Pawn> pawns;
		Pawn winner;
		int turnsCounter;
		Dice dice;
		int sides;
		int numberOfPawns;


	public:

		Board(int sides) {
			this->turnsCounter = 0;
			this->dice=Dice(sides);
		}

		~Board()
		{
			cout<<"Board has been deleted"<<endl;
		}

		void setNumberOfPawns() {
			numberOfPawns = rand()%8+3;
		}

	  int getNumberOfPawns()
        {
        	return this->numberOfPawns;
		}

		void addPawn(string name) {
			pawns.push_back(Pawn(name));
		}

		void addPawns(int numberOfPawns) {
			for (int i=0; i<numberOfPawns; i++) {
				string name;
				cout<<"Give a pawn name: "<<endl;
				cin>>name;
				addPawn(name);

			}
		}

		void setMaxPosition() {
			cout<<"Give max position in the game:"<<endl;
			cin>>this->maxPosition;
			if (maxPosition<1000 || maxPosition>5000) {
				throw "Wrong number of fields";
			}
		}
		Pawn getWinner() {
			return winner;
		}

		void performTurn() {
			this->turnsCounter++;

			ostringstream ss;
			ss << "Turn " << this->turnsCounter;
			Log::info();
			Log::info(ss.str());

			for(int i = 0; i <this->numberOfPawns; i++) {
				int rollResult = this->dice.roll();
				Pawn &pawn = this->pawns.at(i);

			if (pawn.getPosition()%10==0)
			{	cout << "Position is multiple of 10!" << endl;
				while(rollResult==this->dice.getSides())
				{
					cout<<"The maximum number was thrown away"<<endl;
					int bonus = this->dice.roll();
					rollResult+=bonus;
				}
			}

			if (rollResult==1 && pawn.getPosition()%2==1)
			{	cout<<"1 was thrown away and pawn is on odd field, so you have penalty throw"<<endl;
				int penalty=this->dice.roll();
				pawn.subtractFromPosition(penalty);
				if (pawn.getPosition()<0)
				pawn.setPosition(0);
			}

			else
				pawn.addToPosition(rollResult);


				ostringstream ss;
				ss << pawn.getName() << " new position: " << pawn.getPosition();
				Log::info(ss.str());

				if(pawn.getPosition() >= this->maxPosition) {
					this->winner = pawn;
					throw "Winner was called!";
				}
			}
		}
};


int main() {

	srand(time(NULL));

	int sides=0;

	do {
		cout<<"Give number of dice sides :"<<endl;
		cin>>sides;

	} while (sides<4 || sides>15 || sides%2==1);


	Board board = Board(sides);


	board.setNumberOfPawns();
	cout<<"There are "<<board.getNumberOfPawns()<<" players in the game"<<endl;
	board.addPawns(board.getNumberOfPawns());

	try {
		board.setMaxPosition();
	}

	catch(const char* exception) {
		Log::info();
		Log::info("Give correct number of fields (from 1000 to 5000)");
		board.setMaxPosition();

	}

	try {
		while(true) {
			board.performTurn();
		}
	} catch(const char* exception) {
		Log::info();
		Log::info(board.getWinner().getName() + " won");
	}

	return 0;
}



