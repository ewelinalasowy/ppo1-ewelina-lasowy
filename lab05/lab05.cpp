#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <string>

using namespace std;

class Book
{
protected:
    string title;
    string authors;
    int numberOfPages;
    string publishingHouse;
    string revision;

public:
    Book(string title, string authors, int numberOfPages, string publishingHouse, string revision)
    {
        this->title = title;
        this->authors=authors;
        this->numberOfPages=numberOfPages;
        this->publishingHouse=publishingHouse;
        this->revision=revision;
    }

    string getTitle()
    {
        return title;
    }

    string getAuthors()
    {
        return authors;
    }

    int getNumberOfPages()
    {
        return numberOfPages;
    }

    string getPublishingHouse()
    {
        return publishingHouse;
    }

    string getRevision()
    {
        return revision;
    }

    virtual string getType() const = 0;
};

class Novel : public Book
{
private:
    string typeOfNovel;
public:
    Novel(string title, string typeOfNovel, string authors, int numberOfPages, string publishingHouse, string revision) : Book(title,authors,numberOfPages,publishingHouse,revision)
    {

        this->typeOfNovel=typeOfNovel;
    }

    string getType() const
    {
        return "novel";
    }

    string getTypeOfNovel()
    {
        return typeOfNovel;
    }

};

class ComicBook : public Book
{

private:
    string designer;
    int numberOfTom;
public:
    ComicBook (string title,string authors, int numberOfPages, string publishingHouse, string revision, string designer, int numberOfTom) : Book(title,authors,numberOfPages,publishingHouse,revision)
    {
        this->designer = designer;
        this->numberOfTom=numberOfTom;

    }

    string getType() const
    {
        return "comic book";
    }


    string getDesigner()
    {
        return designer;
    }

    int getNumberOfTom()
    {
        return numberOfTom;
    }


};

class Textbook : public Book
{
private:
    int yearOfAdmission;
    string numberOfAdmission;
public:
    Textbook (string title, string authors, int numberOfPages, string publishingHouse, string revision, int yearOfAdmission, string numberOfAdmission) : Book(title,authors,numberOfPages,publishingHouse,revision)
    {
        this->yearOfAdmission=yearOfAdmission;
        this->numberOfAdmission=numberOfAdmission;
    }

    string getType() const
    {
        return "textbook";
    }

    int getYearOfAdmission()
    {
        return yearOfAdmission;
    }

    string getNumberOfAdmission()
    {
        return numberOfAdmission;
    }

};

class Library
{
private:
    vector<Novel> novels;
    vector<ComicBook> comicBooks;
    vector<Textbook> textbooks;

public:
    Library* addNovel(Novel novel)
    {
        this->novels.push_back(novel);
        return this;
    }


    void enterNovel()
    {

        cout<<"Give title"<<endl;
        string title;
        cin.sync();
        getline(cin,title);
        cout<<"Give type of novel"<<endl;
        string typeOfNovel;
        cin.sync();
        getline(cin,typeOfNovel);
        cout<<"Give authors"<<endl;
        string authors;
        cin.sync();
        getline(cin,authors);
        cout<<"Give number of pages"<<endl;
        int numberOfPages;
        cin>>numberOfPages;
        cout<<"Give publishing house"<<endl;
        string publishingHouse;
        cin.sync();
        getline(cin,publishingHouse);
        cout<<"Give revision"<<endl;
        string revision;
        cin.sync();
        getline(cin,revision);

        Novel novel = Novel(title,typeOfNovel,authors,numberOfPages,publishingHouse,revision);
        addNovel(novel);

    }


    Library* addComicBook(ComicBook comicBook)
    {
        this->comicBooks.push_back(comicBook);
        return this;
    }


    void enterComicBook()
    {
        cout<<"Give title"<<endl;
        string title;
        cin.sync();
        getline(cin,title);
        cout<<"Give authors"<<endl;
        string authors;
        cin.sync();
        getline(cin,authors);
        cout<<"Give number of pages"<<endl;
        int numberOfPages;
        cin>>numberOfPages;
        cout<<"Give publishing house"<<endl;
        string publishingHouse;
        cin.sync();
        getline(cin,publishingHouse);
        cout<<"Give revision"<<endl;
        string revision;
        cin.sync();
        getline(cin,revision);
        cout<<"Give designer"<<endl;
        string designer;
        cin.sync();
        getline(cin,designer);
        cout<<"Give number of tom"<<endl;
        int numberOfTom;
        cin>>numberOfTom;
        ComicBook comicBook = ComicBook(title,authors,numberOfPages,publishingHouse,revision,designer,numberOfTom);
        addComicBook(comicBook);
    }


    Library* addTextbook(Textbook textbook)
    {
        this->textbooks.push_back(textbook);
        return this;
    }


    void enterTextbook()
    {

        cout<<"Give title"<<endl;
        string title;
        cin.sync();
        getline(cin,title);
        cout<<"Give authors"<<endl;
        string authors;
        cin.sync();
        getline(cin,authors);
        cout<<"Give number of pages"<<endl;
        int numberOfPages;
        cin>>numberOfPages;
        cout<<"Give publishing house"<<endl;
        string publishingHouse;
        cin.sync();
        getline(cin,publishingHouse);
        cout<<"Give revision"<<endl;
        string revision;
        cin.sync();
        getline(cin,revision);
        cout<<"Give year of admission"<<endl;
        int yearOfAdmission;
        cin>>yearOfAdmission;
        cout<<"Give number of admission"<<endl;
        string numberOfAdmission;
        cin.sync();
        getline(cin,numberOfAdmission);
        Textbook textbook = Textbook(title,authors,numberOfPages,publishingHouse,revision,yearOfAdmission,numberOfAdmission);
        addTextbook(textbook);
    }



    void showNovels()
    {

        for(int i = 0; i < novels.size(); i++)
        {
            Novel novel = novels.at(i);
            cout << novel.getType() << " || " << novel.getTitle() << " "<<novel.getTypeOfNovel()<<" " << novel.getAuthors()<<" "<<novel.getNumberOfPages()<<" "<<novel.getPublishingHouse()<<" "<<novel.getRevision()<<" "<<endl;
        }
    }


    void showTextbooks()
    {
        for(int i = 0; i < textbooks.size(); i++)
        {
            Textbook textbook = textbooks.at(i);
            cout << textbook.getType() << " || " << textbook.getTitle() << " " << textbook.getAuthors()<<" "<<textbook.getNumberOfPages()<<" "<<textbook.getPublishingHouse()<<" "<<textbook.getRevision()<<" "<<textbook.getYearOfAdmission()<<" "<<textbook.getNumberOfAdmission()<<endl;
        }
    }

    void menu()
    {
        cout<<"1 - enter novel"<<endl;
        cout<<"2 - enter comicbook"<<endl;
        cout<<"3 - enter textbook"<<endl;
        cout<<"4 - show all novels"<<endl;
        cout<<"5 - show all comicbooks"<<endl;
        cout<<"6 - show all textbooks"<<endl;
        cout<<"7 - show all books"<<endl;
        cout<<"8 - exit"<<endl;

    }
    void showComicBooks()
    {
        for(int i = 0; i < comicBooks.size(); i++)
        {
            ComicBook comicBook = comicBooks.at(i);
            cout << comicBook.getType() << " || " << comicBook.getTitle() << " " << comicBook.getAuthors()<<" "<<comicBook.getNumberOfPages()<<" "<<comicBook.getPublishingHouse()<<" "<<comicBook.getRevision()<<" "<<comicBook.getDesigner() << " " <<comicBook.getNumberOfTom()<<endl;
        }
    }

    void wypiszWszystko()
    {
        showNovels();
        showTextbooks();
        showComicBooks();

    }

};

int main()
{
    setlocale(LC_ALL, "");
    Library library = Library();

    int option;

    do
    {
        library.menu();
        cin>>option;
        switch (option)
        {
        case 1:
        {
            library.enterNovel();
            break;
        }
        case 2:
        {
            library.enterComicBook();
            break;
        }
        case 3:
        {
            library.enterTextbook();
            break;
        }
        case 4:
        {
            library.showNovels();
            break;
        }
        case 5:
        {
            library.showComicBooks();
            break;
        }
        case 6:
        {
            library.showTextbooks();
            break;
        }
        case 7:
        {
            library.wypiszWszystko();
            break;
        }
        default:
        {
            break;
        }

        }
    }
    while(option>=1 && option<=7);


    return 0;
}



