import random
from force_users import *

quantity = random.randint(5,100)

champions = []

for i in range(quantity):
    randomly=random.randint(0,4)
    if randomly==0:
        champions.append(LightsaberUser("Player "+str(i)))
    elif randomly==1:
        champions.append(LightSideForceUser("Player "+str(i)))
    elif  randomly==2:
        champions.append(DarkSideForceUser("Player "+str(i)))
    elif  randomly==3:
        champions.append(JediMaster("Player "+str(i)))
    elif randomly==4:
        champions.append(SithLord("Player "+str(i)))


print "List of players:"
for w in champions:
    print w.name


loosers =[]


while len(champions) > 1:
    attacker = random.choice(champions)
    target = attacker

    while attacker == target:
        target = random.choice(champions)

    attacker.attack(target)
    if target.life_points <= 0:
        print target.name + " died."
        loosers.append(target.name)
        champions.remove(target)

print ""
print "The winner is " + champions[0].name
print "List of loosers:"
for x in loosers:
    print x