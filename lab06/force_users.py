import random


class ForcePower(object):
    def __init__(self, name, damage):
        self.name = name
        self.damage = damage

    def __str__(self):
        return self.name + ": " + str(self.damage)

class Defence(object):
    def __init__(self,name,defence):
        self.name=name
        self.defence=defence

    def __str__(self):
        return self.name + ": " +str(self.defence)


class ForceUser(object):
    life_points = 100
    available_movements = []
    available_defence = []

    def __init__(self, name):
        self.name = name
        self.available_movements = []
        self.available_defence = []


    def attack(self, opponent):
        drawnMovement = random.choice(self.available_movements)
        defence= random.choice(opponent.available_defence)
        chance_of_defence=random.randint(0,defence.defence)
        if chance_of_defence < 35:
            if_def = 1
        elif chance_of_defence >= 35 & chance_of_defence <= 65:
            if_def = 0.5
        else:
            if_def = 0

        attack_without_defence=drawnMovement.damage*round(random.uniform(0.9,1.1), 2)
        opponent.life_points -= round(attack_without_defence*if_def,2)

        print self.name + " attacks " + opponent.name + " with " + drawnMovement.name + " for " +str(attack_without_defence)
        print "but " + opponent.name +" has " +str(1-if_def) +" chance to defence so after that"
        print opponent.name + " now has " + str(opponent.life_points) + " life points."


class LightsaberUser(ForceUser):
    def __init__(self, name):
        super(LightsaberUser, self).__init__(name)

        self.available_movements.extend([
            ForcePower("Lightsaber Attack", 10),
            ForcePower("Lightsaber Throw", 15)
        ])
        self.available_defence.extend([
                Defence("My defence is 55", 55)
        ])


class LightSideForceUser(ForceUser):
    def __init__(self, name):
        super(LightSideForceUser, self).__init__(name)

        self.available_movements.extend([
            ForcePower("Mind Trick", 5)
        ])

        self.available_defence.extend([
            Defence("My defence is 60", 60)
        ])


class JediMaster(LightsaberUser, LightSideForceUser):
    def __init__(self, name):
        super(JediMaster, self).__init__(name)

        self.available_movements.extend([
            ForcePower("Force Light", 35),
        ])

        self.available_defence.extend([
            Defence("My defence is 80", 80)
        ])

class DarkSideForceUser(ForceUser):
    def __init__(self, name):
        super(DarkSideForceUser, self).__init__(name)

        self.available_movements.extend([
            ForcePower("Force Grip", 15),
            ForcePower("Force Lightning", 15)
        ])
        self.available_defence.extend([
            Defence("My defence is 55", 55)
        ])


class SithLord(LightsaberUser, DarkSideForceUser):
    def __init__(self, name):
        super(SithLord, self).__init__(name)

        self.available_movements.extend([
            ForcePower("Force Rage", 30),
        ])
        self.available_defence.extend([
            Defence("My defence is 85", 85)
        ])


