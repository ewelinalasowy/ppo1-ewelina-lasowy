#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

#define N 5

class Point {
	public:
		float x;
		float y;

		Point() {
			cout << "Point has been created." << endl;
		}

		Point(float x, float y) {
			this->x = x;
			this->y = y;
			cout << "Point [" << this->x << ", " << this->y << "] has been created." << endl;
		}

		~Point() {
			cout << "Point [" << this->x << ", " << this->y << "] has been deleted." << endl;
		}

		void movePoint(float xAxisShift, float yAxisShift) {
			this->x += xAxisShift;
			this->y += yAxisShift;
		}


};

class Square {
	public:
		Point topRight;
		Point topLeft;
		Point bottomLeft;
		Point bottomRight;
		int side;

		Square(Point topRight, int side)
		{
			this->topRight=topRight;
			this->side=side;
			setSides();

		}

		~Square()
		{
			cout<<"Square has been deleted."<<endl;
		}

		void setSides()
		{
			topLeft = Point(topRight.x-side,topRight.y);
			bottomLeft = Point(topRight.x-side,topRight.y-side);
			bottomRight = Point(topRight.x,topRight.y-side);
		}

		void showPoints()
		{
			cout<<"Top right: "<<topRight.x<< " "<<topRight.y<<endl;
			cout<<"Top left: "<<topLeft.x<< " "<<topLeft.y<<endl;
			cout<<"Bottom left: "<<bottomLeft.x<< " "<<bottomLeft.y<<endl;
			cout<<"Bottom right: "<<bottomRight.x<< " "<<bottomRight.y<<endl;
		}


};


class Circle {
	public:
		Point center;
		float radius;

		Circle(Point center, float radius) {
			this->center = center;
			this->radius = radius;
		}

		void getCoordinates() {
			cout << "x: " << this->center.x << endl << "y: " << this->center.y << endl;
		}
};


int main() {
	srand(time(NULL));

	float inputX = 0, inputY = 0;
	float inputRadius = 5;

	Point centerPoint = Point(inputX, inputY);;
	Circle circle = Circle(centerPoint, inputRadius);

	circle.center.movePoint(rand() % 10, rand() % 10);
	circle.getCoordinates();



	for (int i=0; i<N; i++)
	{
		cout<<endl<<"Square no. "<<i+1<<endl;
		Point startPoint = Point (rand()%10, rand()%10);
		int side = rand()%10+1;
		cout<<"Side "<<side<<endl<<endl;
		Square square = Square (startPoint, side);
		square.showPoints();
		cout<<endl<<endl;
	}


	return 0;
}
