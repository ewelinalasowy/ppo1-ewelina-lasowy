package ParkingLotManager.Entities;

public class TeacherCar extends Car {

    public TeacherCar(String plate) {
        super(plate);
    }

    public String identify() {
        return "Teacher car with plate number " + plate;
    }
    
}
