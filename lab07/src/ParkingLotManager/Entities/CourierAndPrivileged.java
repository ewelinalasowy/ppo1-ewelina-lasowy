package ParkingLotManager.Entities;

public class CourierAndPrivileged extends Car {

    public CourierAndPrivileged(String plate) {
        super(plate);
    } 
    
    public String identify() {
        return "Courier or privileged car with plate number " + plate;
    }

}
