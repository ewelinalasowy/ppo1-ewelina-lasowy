package ParkingLotManager;

import ParkingLotManager.Entities.Bicycle;
import ParkingLotManager.Entities.Car;
import ParkingLotManager.Entities.Pedestrian;
import ParkingLotManager.Entities.TeacherCar;
import ParkingLotManager.Entities.Tank;
import ParkingLotManager.Entities.CourierAndPrivileged;
import ParkingLotManager.Interfaces.EntityInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class QueueGenerator {


    public static ArrayList<EntityInterface> generate() {
        ArrayList<EntityInterface> queue = new ArrayList<>();

        final int ANONYMOUS_PEDESTRIANS_COUNT =getRandomNumber(50);
        final int PEDESTRIANS_COUNT = getRandomNumber(5);
        final int CARS_COUNT = getRandomNumber(30);
        final int TEACHER_CARS_COUNT = getRandomNumber(7);
        final int BICYCLES_COUNT = getRandomNumber(3);
        final int TANKS_COUNT=getRandomNumber(1);
        final int COURIERS_AND_PRIVILEGED_COUNT=getRandomNumber(2);
       
        
        for (int i = 0; i <= ANONYMOUS_PEDESTRIANS_COUNT; i++) {
            queue.add(new Pedestrian());
        }

        for (int i = 0; i <= PEDESTRIANS_COUNT; i++) {
            queue.add(new Pedestrian(getRandomName()));
        }

        for (int i = 0; i <= CARS_COUNT; i++) {
            queue.add(new Car(getRandomPlateNumber()));
        }

        for (int i = 0; i <= TEACHER_CARS_COUNT; i++) {
            queue.add(new TeacherCar(getRandomPlateNumber()));
        }

        for (int i = 0; i <= BICYCLES_COUNT; i++) {
            queue.add(new Bicycle());
        }

        for (int i = 0; i < TANKS_COUNT; i++) {
            queue.add(new Tank());
        }

        for (int i = 0; i < COURIERS_AND_PRIVILEGED_COUNT; i++) {
            queue.add(new CourierAndPrivileged(getRandomPlateNumber()));
        }

        
        Collections.shuffle(queue);
        

        return queue;
    }

    protected static String getRandomPlateNumber() {
        Random generator = new Random();
        return "DLX " + (generator.nextInt(89999) + 10000);
    }
    
    private static String getRandomName() {
        String[] names = {"John", "Jack", "James", "George", "Joe", "Jim"};
        return names[(int) (Math.random() * names.length)];
    }
    
    private static int getRandomNumber (int n)
    {
    	Random generator = new Random();
    	return generator.nextInt(n);
    }

}
