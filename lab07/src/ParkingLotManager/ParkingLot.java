package ParkingLotManager;

import ParkingLotManager.Entities.Car;
import ParkingLotManager.Entities.TeacherCar;
import ParkingLotManager.Entities.Bicycle;
import ParkingLotManager.Entities.Pedestrian;
import ParkingLotManager.Entities.CourierAndPrivileged;
import ParkingLotManager.Interfaces.EntityInterface;


import java.util.ArrayList;

final public class ParkingLot {

    private ArrayList<EntityInterface> entitiesOnProperty = new ArrayList<>();
    private int carsOnProperty = 0;
    private int day_earnings=0;
    private int bicycleOnProperty=0;
    private static final int PRICE_FOR_CARS = 5;
    private static final int PARKING_SPOTS = 200;
    private static final int BIKE_RACKS=15;
    
 
    public boolean checkIfCanEnter(EntityInterface entity) {
        return entity.canEnter();
    }
    
        
	public void letIn(EntityInterface entity) {

		if (entity instanceof Bicycle && bicycleOnProperty < BIKE_RACKS) {
			bicycleOnProperty++;
			let(entity);

		}
		
		
		if (entity instanceof Bicycle && bicycleOnProperty >= BIKE_RACKS) {
			notlet(entity);

		}
		
		else if (entity instanceof Car && !(entity instanceof CourierAndPrivileged) && carsOnProperty < PARKING_SPOTS) {
			
			carsOnProperty++;
			let(entity);
			
		}
		
		else if (entity instanceof Car && !(entity instanceof CourierAndPrivileged) && carsOnProperty >=PARKING_SPOTS) {
			notlet(entity);

		}

		if (entity instanceof Car && !(entity instanceof TeacherCar) && !(entity instanceof CourierAndPrivileged)) {
			day_earnings += PRICE_FOR_CARS;
			
		}
		
		if (entity instanceof Pedestrian || entity instanceof CourierAndPrivileged)
		{
			let(entity);
		}

	}
    	
	public void let(EntityInterface entity)
	{
		entitiesOnProperty.add(entity);
		Log.info(entity.identify() + " let in.");
	}
	
	public void notlet(EntityInterface entity)
	{
		Log.info(entity.identify() + " not let in.");
	}

    public int countCars() {
        return carsOnProperty;
    }
    
    public int countMoney()
    {
    	return day_earnings;
    }

}
