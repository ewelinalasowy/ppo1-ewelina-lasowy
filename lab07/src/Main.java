import ParkingLotManager.Interfaces.EntityInterface;
import ParkingLotManager.Log;
import ParkingLotManager.ParkingLot;
import ParkingLotManager.QueueGenerator;

import java.time.LocalTime;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<EntityInterface> queue = QueueGenerator.generate();
        ParkingLot parking = new ParkingLot();

        Log.info("There's " + parking.countCars() + " cars in the parking lot");
        Log.info();
        
        
        LocalTime localTime1 = LocalTime.of(8,0,0);
        for (int i =0; i<7; i++) {
        for (EntityInterface entity : queue) {
            if(parking.checkIfCanEnter(entity)) {
                parking.letIn(entity); 
                Log.info("Time: "+ localTime1);
                localTime1 = localTime1.plusSeconds(30);
            }
        }
        localTime1=localTime1.plusMinutes(70);
        Log.info();
        queue = QueueGenerator.generate();
        
        }
        Log.info();
        Log.info("There's " + parking.countCars() + " cars in the parking lot");
        Log.info("Day's earnings = "+ parking.countMoney());
    }
}
