package Zoo.Animals;

public abstract class Omnivorous extends Animal {

	Omnivorous (String name)
	{
		super(name);
	}
	
	  String[] getDiet() {
	        return new String[]{"vegetables","fruits","leaves","meat","fish","water"};
	    }
}
