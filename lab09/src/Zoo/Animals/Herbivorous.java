package Zoo.Animals;

public abstract class Herbivorous extends Animal {

	Herbivorous(String name)
	{
		super(name);
	}
	
	
    String[] getDiet() {
        return new String[]{"vegetables","fruits","leaves","water"};
    }
}
