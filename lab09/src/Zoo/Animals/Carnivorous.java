package Zoo.Animals;

public abstract class Carnivorous extends Animal {

	Carnivorous(String name)
	{
		super(name);
		
	}
	
	
    String[] getDiet() {
        return new String[]{"meat","fish","water"};
    }
}
