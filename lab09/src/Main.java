import Zoo.Animals.Chimpanzee;
import Zoo.Animals.Dolphin;
import Zoo.Animals.Elephant;
import Zoo.Animals.Giraffe;
import Zoo.Animals.Lion;
import Zoo.Animals.Penguin;
import Zoo.Animals.Porcupine;
import Zoo.Animals.Tiger;
import Zoo.Log;

import java.time.LocalTime;

import Food.Food;
import Zoo.Zoo;

public class Main {

    public static void main(String[] args) {
        Zoo zoo = new Zoo("Zoo Legnica");

        zoo.addAnimal(new Lion("Simba"))
            .addAnimal(new Lion("Mufasa"))
            .addAnimal(new Elephant("Dumbo"))
            .addAnimal(new Chimpanzee("Tytus"))
            .addAnimal(new Dolphin("Meg"))
            .addAnimal(new Giraffe ("Melman"))
            .addAnimal(new Penguin("Kowalski"))
            .addAnimal(new Porcupine("Porcupine"))
            .addAnimal(new Tiger("Shere Khan"));


        LocalTime localTime1 = LocalTime.of(8,0,0);
        for (int i=0; i<2; i++)
        {
        	switch (i) {
        		case 0:
        		{
        			Log.info();
        			Log.info("Time: "+ localTime1);
        			zoo.feedAnimals(Food.Water());
        			localTime1 = localTime1.plusMinutes(60);
        		}
        		
        		case 1:
        		{
        			Log.info();
            			Log.info("Time: "+ localTime1);
            			zoo.feedAnimals(Food.Vegetables());
            			localTime1 = localTime1.plusMinutes(60);
        			
        		}
        		
        		case 2:
        		{
        			Log.info();
        			Log.info("Time: "+ localTime1);
        			zoo.feedAnimals(Food.Fruits());
        			localTime1 = localTime1.plusMinutes(60);
        			
        		}
        			
        		case 3:
        		{
        			Log.info();
        			Log.info("Time: "+ localTime1);
        			zoo.feedAnimals(Food.Leaves());
        			localTime1 = localTime1.plusMinutes(60);
        			
        		}
        		
        		case 4:
        		{
        			Log.info();
        			Log.info("Time: "+ localTime1);
        			zoo.feedAnimals(Food.Meat());
        			localTime1 = localTime1.plusMinutes(60);
        			
        		}
        			
        		case 5:
        		{
        			Log.info();
        			Log.info("Time: "+ localTime1);
        			zoo.feedAnimals(Food.Fish());
        			localTime1 = localTime1.plusMinutes(60);
        			
        		}
        		
        		default: {} 
        		
        	}
        }
        
        
   
      
    } 
}
