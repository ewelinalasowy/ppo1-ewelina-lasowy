#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

#define STUDENTS_COUNT 10

class Student {
	public:
		string studentNo;
		string studentSurname;
		string studentName;
		bool studentActivity;
			
		void setStudentNo(string studentNo) {
			this->studentNo = studentNo;
		}
		
			string getStudentNo() {
			return this->studentNo;
		}
		
		string getStudentSurname() {
			return this->studentSurname;
		}
		
			void setStudentSurname(string studentSurname) {
			this->studentSurname = studentSurname;
		}
		
		
	
		string getStudentName() {
			return this->studentName;
		}
		
			void setStudentName(string studentName) {
			this->studentName = studentName;
		}
		
		
		
		
		void setStudentActivity(bool studentActivity) {
			this->studentActivity = studentActivity;
		}
		
		bool getStudentActivity() {
			return this->studentActivity;
		}
		
		
};

string getRandomStudentNumber() {
	ostringstream ss;
	int randomNumber = rand() % 2000 + 37000;
	
	ss << randomNumber;
	
	return ss.str();
}

bool getRandomStudentActivity() {

return (rand()%2>0);
}


string getRandomStudentSurname() {
	ostringstream ss;
	int randomNumber = rand() % 6;
	string surnames[7]={"Nowak","Kowalski","Abacka","Cabacki","Kowal","Mysz","Zimny"};
	ss << surnames[randomNumber];
	
	return ss.str();
}

string getRandomStudentName() {
	ostringstream ss;
	int randomNumber = rand() % 6;
	string names[7]={"Anna","Maria","Jan","Szymon","Tomasz","Ewelina","Izabela"};
	ss << names[randomNumber];
	
	return ss.str();
}

int main() {
	vector<Student> students;
	
	for(int i = 0; i < STUDENTS_COUNT; i++) {
		Student student;
		student.setStudentNo(getRandomStudentNumber());
		student.setStudentSurname(getRandomStudentSurname());
		student.setStudentName(getRandomStudentName());
		student.setStudentActivity(getRandomStudentActivity());
		students.push_back(student);
	}
	
	cout  << "Students group have been filled." << endl << endl;
	
	for(int i = 0; i < students.size(); i++) {
		Student student = students.at(i);
		if (student.getStudentActivity()==true){
		cout <<student.getStudentSurname()<<" ";
		cout <<student.getStudentName()<<" ";
		cout <<"(" << student.getStudentNo() <<")" <<endl;
	}
		
	}
	
	return 0;
}
