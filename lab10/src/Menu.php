<?php

namespace PPO\Notebook;


use PPO\Notebook\Entries\DrankBeer;
use PPO\Notebook\Entries\WatchedMovie;
use PPO\Notebook\Entries\PlayedGame;
use PPO\Notebook\Entries\BakedCake;
use PPO\Notebook\Entries\ReadBook;

final class Menu
{

    private $notebook;

    public function __construct(Notebook $notebook)
    {
        $this->notebook = $notebook;
    }

    public function run(): void
    {
        $running = true;

        $list = [
            "1" => "listNotes",
            "2" => "saveWatchedMovie",
            "3" => "saveDrankBeer",
            "4" => "savePlayedGame",
            "5" => "saveBakedCake",
            "6" => "saveReadBook",
			"x" => "exitNotebook"

        ];
        while ($running) {
            $this->displayMenu();

                $key= readline("Podaj opcje: ");

				if(isset($list[$key]))
				{
				    $methodName=$list[$key];

				    try {
                        $this->$methodName();
                    }

                    catch (\Exception $e)
                    {
                        $running=false;
                    }
				}



        }
    }

	private function listNotes():void{

				$this->write();
                $this->write("zanotowano:", true);
                foreach ($this->notebook->getAll() as $entry) {
                    $this->write($entry->getSlug());
                }
	}

	private function saveWatchedMovie():void {

				$title = readline("Podaj tytuł filmu: ");
                $movie = new WatchedMovie($title);
                $this->notebook->save($movie);
	}

	private function saveDrankBeer():void {

				$name = readline("Podaj nazwę piwa: ");
                $brewery = readline("Podaj nazwę browaru: ");
                $beer = new DrankBeer($name, $brewery);
                $this->notebook->save($beer);

	}

	private function savePlayedGame():void {

				$titleOfGame = readline("Podaj tytuł gry: ");
                $yearOfProduction = readline("Podaj rok wydania gry: ");
                $game = new PlayedGame($titleOfGame, $yearOfProduction);
                $this->notebook->save($game);
	}

	private function saveBakedCake():void {

				$nameOfCake = readline("Podaj nazwe ciasta, ktore upiekles: ");
                $cake = new BakedCake($nameOfCake);
                $this->notebook->save($cake);

	}

	private function saveReadBook():void {

				$titleOfBook = readline("Podaj tytul przeczytanej ksiazki: ");
                $author = readline("Podaj autora przeczytanej ksiazki");
                $book = new ReadBook($titleOfBook, $author);
                $this->notebook->save($book);
	}

	private function exitNotebook():bool{
               throw new \Exception("Exit") ;
	}

    private function displayMenu(): void
    {
        $this->write();
        $this->write("notatek: {$this->notebook->countEntries()}", true);
        $this->write("[1] wypisz notatki");
        $this->write("[2] zanotuj obejrzany film");
        $this->write("[3] zanotuj wypite piwo");
        $this->write("[4] zanotuj gre, w ktora grales");
        $this->write("[5] zanotuj ciasto, ktore upiekles");
        $this->write("[6] zanotuj przeczytana ksiazke");
        $this->write("[x] wyjdź");
    }


    private function write(string $line = "", $comment = false): void
    {
        if ($comment) echo "::: ";
        echo $line . PHP_EOL;
    }


}