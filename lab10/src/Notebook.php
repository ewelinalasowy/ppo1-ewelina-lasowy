<?php

namespace PPO\Notebook;

use PPO\Notebook\Interfaces\NotebookEntry;

final class Notebook {

	private $entries = [];

	public function save(NotebookEntry $entry): self {

	    $this->entries[] = $entry;
            return $this;


	}

	public function countEntries(): int {
		return count($this->entries);
	}

	/**
	 * @return NotebookEntry[]
	 */
	public function getAll(): array {

	    $collection=collect($this->entries);
	    $sorted = $collection->sort(function(NotebookEntry $a, NotebookEntry $b){
	        return $a->getSlug() > $b->getSlug();
        });
	    return $sorted->values()->all();
	}

}
