<?php

namespace PPO\Notebook\Entries;

use PPO\Notebook\Interfaces\NotebookEntry;
use PPO\Notebook\Traits\Slugger;

class PlayedGame implements NotebookEntry {

    use Slugger;

    private $titleOfGame;
    private $yearOfProduction;

    public function __construct(string $titleOfGame, $yearOfProduction) {
        $this->titleOfGame= $titleOfGame;
        $this->yearOfProduction = $yearOfProduction;
    }

    protected function getSlugBase(): string {
        return $this->titleOfGame . " " . $this->yearOfProduction;
    }

}
